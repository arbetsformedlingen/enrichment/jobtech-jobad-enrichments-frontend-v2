# JobtechJobadEnrichmentsFrontendV2

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 17.0.6

## Install local development environment
1. Follow   
   https://angular.io/guide/setup-local#prerequisites
   https://angular.io/guide/setup-local#install-the-angular-cli
   ...to install nodeJS and the Angular CLI
2. cd to root folder, jobtech-jobad-enrichments-frontend-v2
   Run command: npm install
3. To start local server, run command: ng serve
   Navigate to `http://localhost:7012/`
4. To start debug functionality, add querystring `?debug=true`, for example: `http://localhost:7012/?debug=true`  

The application will automatically reload in the browser if you change any of the source files.

## Build
Run `ng build` to build the project. The build artifacts will be stored in the `jobtech-jobad-enrichments-frontend-v2/dist/` directory.

## Docker  

### Build
podman build -t jae-frontend-v2:latest .

Note: If jobtech-jobad-enrichments-frontend-v2/dist directory is missing, cd to jobtech-jobad-enrichments-frontend-v2 and execute:
`ng build` since the `dist` directory is needed to build the docker file.

### Run
podman run -d -p 8080:8080 --name jae-frontend-v2 jae-frontend-v2  

Navigate to `http://localhost:8080/`

### Check log file
podman logs jae-frontend-v2 --details -f

### Stop & remove image
podman stop jae-frontend-v2 && podman rm jae-frontend-v2 || true

### Debug Docker
podman logs jae-frontend-v2 --details -f  
podman exec -t -i jae-frontend-v2 /bin/bash  
podman run -it --rm jae-frontend-v2:latest  

### Update Angular to latest minor version
Run:
`npm install`
`ng update @angular/cli@^17 @angular/core@^17 @angular/cdk@^17 @angular/material@^17`

In case of errors when updating material or cdk, search for the latest minor version and set exact version to install:  
`ng update @angular/material@17.3.10`
`ng update @angular/cdk@17.3.10`

### List installed packages and versions
npm list --depth=0

## Urls for further development
https://angular.io/cli/generate
https://ng-bootstrap.github.io
http://tachyons.io/#style
https://fontawesome.com/icons/categories/design
