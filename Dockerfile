FROM node:22.6.0-alpine3.20 AS build

WORKDIR /usr/src/app
COPY . .

RUN npm install -g npm@10.8.2

RUN npm install
# Note: Remove step 'RUN npm run build' when building with podman in local dev environment,
# to avoid error: 'sh: ng: Permission denied - Error: building at STEP "RUN npm run build": while running runtime: exit status 126'
RUN npm run build

FROM nginxinc/nginx-unprivileged:1.25.3-alpine3.18-slim
RUN rm /etc/nginx/conf.d/default.conf
COPY --from=build /usr/src/app/dist/jobtech-jobad-enrichments-frontend-v2 /usr/share/nginx/html
COPY nginx.conf /etc/nginx/conf.d/default.conf
EXPOSE 8080
CMD ["nginx", "-g", "daemon off;"]
