import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DebugService {

  constructor() { }

  isDebug: boolean = false;

  public getIsDebug(): boolean {
    return this.isDebug;
  }

  public setIsDebug(isDebugValue: boolean) {
    console.log("DebugService. Setting isDebug to: " + isDebugValue);
    this.isDebug = isDebugValue;
  }

}
