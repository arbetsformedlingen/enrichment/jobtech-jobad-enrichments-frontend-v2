import {Injectable, OnInit} from '@angular/core';
import {catchError, Observable, throwError} from 'rxjs';
import {map} from 'rxjs/operators';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {AdInput} from "../model/ad-input.model";
import {EnrichInput} from "../model/enrich-input.model";
import {EnrichOutput} from "../model/enrich-output.model";


export class SynonymTypes {
  competencies: string[]  = [];
  geos: string[]  = [];
  occupations: string[]  = [];
  traits: string[]  = [];
}

let enrichedCandidatesSynonyms: SynonymTypes[] = [];
let enrichedCandidatesMisspelledSynonyms: SynonymTypes[] = [];

const ENV_LOCALHOST = 'localhost';
const ENV_TEST = 'test';
const ENV_PROD = 'prod';
const ENV_ONPREM_TEST = 'onprem-test';
const ENV_ONPREM_PROD = 'onprem-prod';

const ENRICH_URL_LOCALHOST = 'http://localhost:6358';
const ENRICH_URL_TEST = 'https://jobad-enrichments-test-api.jobtechdev.se';
const ENRICH_URL_PROD = 'https://jobad-enrichments-api.jobtechdev.se';
const ENRICH_URL_ONPREM_TEST = 'https://jobad-enrichments-api-onprem-test.jobtechdev.se';
const ENRICH_URL_ONPREM_PROD = 'https://jobad-enrichments-api-onprem.jobtechdev.se';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  public enrichBaseUrl: string = "";
  public environmentName: string = "";

  private randomJobListing: any;
  private docIdAdInput: any;
  private enrichOutput: any;

  public synonymdata: any = {
    "enriched_candidates_synonyms": enrichedCandidatesSynonyms,
    "enriched_candidates_misspelled_synonyms": enrichedCandidatesMisspelledSynonyms
  };

  constructor(private httpClient: HttpClient) {
    this.environmentName = DataService.getCurrentFrontendEnvironment();
    console.log("DataService. Set environmentName to: " + this.environmentName)
    this.enrichBaseUrl = this.getCurrentEnrichBaseUrl();
    console.log("DataService. Set enrichBaseUrl to: " + this.enrichBaseUrl)
  }


  private static getHostname() {
    let hostname = window.location.hostname;
    console.log('window.location.hostname:' + hostname);
    return hostname;
  }

  public showBinarySynonyms: boolean = false;


  private static getCurrentFrontendEnvironment(): string {
    let hostname = DataService.getHostname();
    let currentEnvironment;

    if (hostname === 'localhost' || hostname === '0.0.0.0' || hostname === '127.0.0.1') {
      // currentEnvironment = ENV_LOCALHOST;
      currentEnvironment = ENV_PROD;
    } else if (hostname.includes('test.jobtechdev.se')) {
      currentEnvironment = ENV_TEST;
    } else if (hostname.includes('onprem.jobtechdev.se')) {
      currentEnvironment = ENV_ONPREM_PROD;
    } else if (hostname.includes('onprem-test.jobtechdev.se')) {
      currentEnvironment = ENV_ONPREM_TEST;
    } else {
      currentEnvironment = ENRICH_URL_PROD;
    }
    return currentEnvironment;
  }

  public getCurrentEnrichBaseUrl(): string {
    let currentEnrichBaseUrl;

    if (this.environmentName === ENV_LOCALHOST) {
      currentEnrichBaseUrl = ENRICH_URL_LOCALHOST;
    } else if (this.environmentName === ENV_TEST) {
      currentEnrichBaseUrl = ENRICH_URL_TEST;
    } else if (this.environmentName === ENV_ONPREM_PROD) {
      currentEnrichBaseUrl = ENRICH_URL_ONPREM_PROD;
    } else if (this.environmentName === ENV_ONPREM_TEST) {
      currentEnrichBaseUrl = ENRICH_URL_ONPREM_TEST;
    } else {
      currentEnrichBaseUrl = ENRICH_URL_PROD;
    }
    return currentEnrichBaseUrl;

  }

  public getRandomJobListing(): Observable<AdInput> {
    // public getRandomJobListing(): void {
    console.log(`DataService.getRandomJobListing`)

    const url = `${this.enrichBaseUrl}/demo_jobads/random`;

    console.log(`DataService.getRandomJobListing. Calling endpoint with url: ${url}`)

    return this.httpClient
      .get(url).pipe(catchError(this.handleError))
      .pipe(map(data => this.randomJobListing = DataService.handleAdInputResponse(data)));


  }

  enrichText(enrichInput: EnrichInput): Observable<EnrichOutput> {

    let documentsInput:any[] = [];
    let input_data = {
      include_sentences: true,
      include_terms_info: true,
      include_synonyms: false,
      include_misspelled_synonyms: false,
      documents_input: documentsInput,
      debug: true
    };

    let input_doc = {
      doc_id: enrichInput.doc_id_input,
      doc_headline: enrichInput.doc_headline_input,
      doc_text: enrichInput.doc_text_input,
    };

    if (enrichInput.is_binary_enrichment) {
      input_data.include_synonyms = true;
      input_data.include_misspelled_synonyms = true;
    }

    input_data.documents_input[0] = input_doc;

    console.log(`input_data: ${input_data}`);

    console.log('DataService.enrichText')
    const endpointname = enrichInput.is_binary_enrichment ? 'enrichtextdocumentsbinary': 'enrichtextdocuments';
    const url = `${this.enrichBaseUrl}/${endpointname}`;

    console.log(`DataService.enrichText. Calling endpoint with url: ${url}`)

    return this.httpClient
      .post(url, input_data).pipe(catchError(this.handleError))
      .pipe(map(data => this.enrichOutput = DataService.handleEnrichOutputResponse(data)));
  }

  private static handleEnrichOutputResponse(data: any) {
    let enrichOutput: EnrichOutput = new EnrichOutput();
    enrichOutput.data = data;
    console.log(`DataService.handleEnrichOutputResponse : ${enrichOutput}`)

    return enrichOutput;
  }

  getDemoHtmlJobAdByDocIdInput(doc_id_input: string | undefined): Observable<AdInput> {
    console.log(`DataService.getDemoHtmlJobAdByDocIdInput`)
    const url = `${this.enrichBaseUrl}/demo_jobads/htmlads/${doc_id_input}`;

    console.log(`DataService.getDemoHtmlJobAdByDocIdInput. Calling endpoint with url: ${url}`)

    return this.httpClient
      .get(url).pipe(catchError(this.handleError))
      .pipe(map(data => this.docIdAdInput = DataService.handleAdInputResponse(data)));

  }

  private static handleAdInputResponse(data: any) {
    let adInput: AdInput = new AdInput();
    adInput.doc_id_input = data['doc_id']
    adInput.doc_headline_input = data['doc_headline']
    adInput.doc_text_input = data['doc_text']
    console.log("DataService.handleAdInputResponse. Got ad data!");
    // console.log("DataService.handleAdInputResponse : " + JSON.stringify(adInput));

    return adInput;
  }

  private handleError(error: HttpErrorResponse) {
    if (error.status === 0) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong.
      console.error(
        `Backend returned code ${error.status}, body was: `, error.error);
    }
    // Return an observable with a user-facing error message.
    return throwError(() => new Error('Something bad happened; please try again later.'));
  }


}
