import {Component, OnInit} from '@angular/core';
import {faBookReader, faDice, faFileExport, faFileImport} from '@fortawesome/free-solid-svg-icons';
import {DataService} from "../services/data.service";
import {AdInput} from "../model/ad-input.model";
import {EnrichInput} from "../model/enrich-input.model";
import {EnrichOutput} from "../model/enrich-output.model";
import {TextPart} from "../model/text-part.model";
import {DebugService} from "../services/debug.service";

@Component({
  selector: 'app-enrich',
  templateUrl: './enrich.component.html',
  styleUrls: ['./enrich.component.sass']
})
export class EnrichComponent implements OnInit {


  constructor(public dataService: DataService, private debugService: DebugService) {
  };

  ngOnInit(): void {
  }

  faBookReader = faBookReader
  faFileExport = faFileExport
  faFileImport = faFileImport
  faDice = faDice

  isExamplesCollapsed = true;
  isShowSentencesVisible = false;
  isSentencesCollapsed = true;
  isBinaryEnrichment = false;

  enrichingstatusInProgress = false;

  doc_id_input: string = "";
  // doc_id_input: string = "8356374-AF";
  doc_headline_input: string = "";
  doc_text_input: string = "";

  output_enriched_text: TextPart[] = [];
  output_enriched_headline: TextPart[] = [];

  outputdata = {
    "enrichedText": this.output_enriched_text,
    "enrichedHeadline": this.output_enriched_headline,
    "doc_id": "",
    "html_rewritten_words": ""
  };

  showEnrichedData(): boolean {
    return this.outputdata.enrichedHeadline.length > 0 || this.outputdata.enrichedText.length > 0
  }

  showSynonymsCheckbox(): boolean {
    return this.isBinaryEnrichment &&
      !this.enrichingstatusInProgress && this.showEnrichedData() &&
      (this.dataService.synonymdata.enriched_candidates_synonyms != undefined || this.dataService.synonymdata.enriched_candidates_misspelled_synonyms != undefined);
  }


  resetEnrichedData() {
    this.outputdata.enrichedHeadline = [];
    this.outputdata.enrichedText = []
    this.outputdata.doc_id = "";
    this.outputdata.html_rewritten_words = "";
  }

  enrichTextFromInput(): void {

    let enrichInput: EnrichInput = new EnrichInput();

    if (this.doc_text_input !== '') {
      this.enrichingstatusInProgress = true;
    }

    enrichInput.doc_id_input = this.doc_id_input;
    enrichInput.doc_headline_input = this.doc_headline_input;
    enrichInput.doc_text_input = this.doc_text_input;
    enrichInput.is_binary_enrichment = false;
    this.isBinaryEnrichment = false;


    this.dataService.enrichText(enrichInput).subscribe(
      enrichOutput => {
        // console.log('enrichTextFromInput, enrichedData:')
        // console.log(enrichedData)
        this.handleEnrichTextReturnValue(enrichInput, enrichOutput);
        this.enrichingstatusInProgress = false;
      }
    );
  }

  enrichTextBinaryFromInput(): void {
    let enrichInput: EnrichInput = new EnrichInput();

    if (this.doc_text_input !== '') {
      this.enrichingstatusInProgress = true;
    }

    enrichInput.doc_id_input = this.doc_id_input;
    enrichInput.doc_headline_input = this.doc_headline_input;
    enrichInput.doc_text_input = this.doc_text_input;
    enrichInput.is_binary_enrichment = true;
    this.isBinaryEnrichment = true;

    this.dataService.enrichText(enrichInput).subscribe(
      enrichOutput => {
        this.handleEnrichTextReturnValue(enrichInput, enrichOutput);
        this.enrichingstatusInProgress = false;
      }
    );
  }

  private handleEnrichTextReturnValue(enrichInput: EnrichInput, enrichOutput: EnrichOutput) {

    let responsedata = enrichOutput.data[0]

    const output = this.buildTextParts(responsedata, enrichInput.doc_text_input, enrichInput.is_binary_enrichment);
    this.outputdata.enrichedText = output.enrichedText;
    this.outputdata.enrichedHeadline = output.enrichedHeadline;
    this.outputdata.doc_id = output.doc_id !== '' ? output.doc_id : enrichInput.doc_id_input;

    if (this.debugService.getIsDebug()) {
      this.isShowSentencesVisible = true;
      console.log("handleEnrichTextReturnValue, this.debugService.getIsDebug() is true. Getting rewritten terms.")
      this.outputdata.html_rewritten_words = this.highlightRewrittenTerms(responsedata);
    }

    this.dataService.synonymdata.enriched_candidates_synonyms = responsedata.enriched_candidates_synonyms;
    this.dataService.synonymdata.enriched_candidates_misspelled_synonyms = responsedata.enriched_candidates_misspelled_synonyms;

  }


  private buildTextParts(reponsedata: any, text: string | undefined, isBinaryEnrichment: boolean) {
    const CHAR_CODE_NON_BREAKING_SPACE = 160;

    const firstdoc = reponsedata;
    const doc_headline = firstdoc.doc_headline;
    const doc_id: string = firstdoc.doc_id;
    const enriched_candidates = firstdoc.enriched_candidates;
    const competencies = enriched_candidates.competencies;
    const occupations = enriched_candidates.occupations;
    const traits = enriched_candidates.traits;
    const geos = enriched_candidates.geos;

    let all_candidates: any[] = [];
    all_candidates = all_candidates.concat(competencies);
    all_candidates = all_candidates.concat(occupations);
    all_candidates = all_candidates.concat(traits);
    all_candidates = all_candidates.concat(geos);

    // console.log(all_candidates)

    const candidate_types_dict: { [key: string]: string } = {
      'FORMAGA': 'TRAIT',
      'KOMPETENS': 'COMPETENCE',
      'YRKE': 'OCCUPATION',
      'GEO': 'GEO'
    };

    const doc_sentences = firstdoc.doc_sentences;

    const doc_sentences_new_words_mappings = firstdoc.doc_sentences_new_words_mappings;

    let enrichedHeadline: TextPart[] = [];
    let enrichedText: TextPart[] = [];

    let outputdata = {
      "doc_headline": "",
      "doc_id": "",
      "enrichedHeadline": enrichedHeadline,
      "enrichedText": enrichedText
    };
    outputdata.doc_headline = doc_headline;
    outputdata.doc_id = doc_id;

    let new_sentence = '';
    let new_text = '';
    for (let i = 0; i < doc_sentences.length; i++) {
      let listToPushTo: TextPart[];
      let orderedCandidates: any[] = [];

      if (i === 0) {
        listToPushTo = enrichedHeadline;
      } else {
        listToPushTo = enrichedText;
      }

      const sentence = doc_sentences[i];
      //Yes, people do put non breaking spaces in their job ads... (hard to find)
      const clean_sentence = sentence.replaceAll(String.fromCharCode(CHAR_CODE_NON_BREAKING_SPACE), ' ');

      //Handle rendering of compound words, for example 'hälso- och sjukvård'
      const sentence_new_words_mappings = doc_sentences_new_words_mappings[i];
      //var persons: { [id: string] : IPerson; } = {};
      let sentence_new_words_mappings_lower: any = {};
      for (let mappingKey in sentence_new_words_mappings) {
        const keyLower = mappingKey.toLowerCase();
        sentence_new_words_mappings_lower[keyLower] = sentence_new_words_mappings[mappingKey].toLowerCase();
      }

      //Hitta alla candidates för denna mening, när de används - sätt flagga used=true.
      let sentence_candidates = all_candidates.filter(function (el) {
        return el.sentence_index === i;
      });

      // console.log(sentence_candidates);

      let new_sentence = clean_sentence;

      for (let j = 0; j < sentence_candidates.length; j++) {
        const candidate = sentence_candidates[j];

        // console.log(candidate);
        let term = candidate.term;
        let rewrittenTerm = '';
        // console.log('Processing term:' + term);

        if (term in sentence_new_words_mappings_lower) {
          // console.log(term +  ', (old):' + sentence_new_words_mappings[term]);
          rewrittenTerm = term;
          term = sentence_new_words_mappings_lower[term];
        }

        const concept = candidate.concept_label;
        const conceptTypeName = candidate.concept_type_name;
        const presentationType = candidate_types_dict[conceptTypeName];

        let search_new_sentence = new_sentence.replace(new RegExp('"', "g"), '@');
        search_new_sentence = search_new_sentence.replace(new RegExp("'", "g"), '€');
        search_new_sentence = search_new_sentence.toLowerCase();
        // console.log('search_new_sentence: ' + search_new_sentence);
        let search_term = term.toLowerCase();
        // Escape special regex-characters before searching for term in sentence.
        search_term = search_term.replace(/[-[\]{}()*+?.,\\^$|]/g, "\\$&");
        // console.log('search_term: ' + search_term);

        const regex_next_term = new RegExp("(^|[^€a-zA-ZåäöÅÄÖ0-9])" + search_term + "([^€a-zA-ZåäöÅÄÖ0-9]|$)");
        // console.log('regex_next_term: ' + regex_next_term);
        const charStart = search_new_sentence.search(regex_next_term);
        // console.log('term: ' + term + ', charStart: ' + charStart);
        const charEnd = charStart + term.length + 1;
        // console.log('term: ' + term + ', charEnd: ' + charEnd);

        const textBeforeTerm = new_sentence.slice(0, charStart);
        const textAfterTerm = new_sentence.slice(charEnd, new_sentence.length);

        // console.log('sentence_index (i): ' + i);
        // console.log('textBeforeTerm: ' + textBeforeTerm);
        // console.log('textAfterTerm: ' + textAfterTerm);
        let prediction;
        if (!isBinaryEnrichment) {
          prediction = candidate.prediction;
        }

        const termMisspelled = candidate.term_misspelled;

        new_sentence = `${textBeforeTerm}
        <termobj vacancy-id="'${doc_id}'" concept="'${concept}'" concept-type-name="'${conceptTypeName}'" presentation-type="'${presentationType}'" prediction="'${prediction}'" rewritten-term="'${rewrittenTerm}'" term-misspelled="'${termMisspelled}'" term="'${term}'" ></termobj>
        ${textAfterTerm}`;

      }

      new_sentence = new_sentence.trim();

      const regex_termobj_included = new RegExp("<termobj.*></termobj>", "g");

      const newHeadlineCandidateMatches = new_sentence.matchAll(regex_termobj_included);
      // console.log(Array.from(newHeadlineCandidateMatches, m => m[0]));
      let lastIndex = 0;
      for (var headlineCandidate of newHeadlineCandidateMatches) {
        let candidateIndex = headlineCandidate.index;
        if (candidateIndex === undefined) {
          candidateIndex = -1;
        }
        let textBeforeTerm = new_sentence.slice(lastIndex, candidateIndex);
        textBeforeTerm = textBeforeTerm.trim()
        if (textBeforeTerm != '') {
          let textBefore = new TextPart();
          textBefore.is_plain_text = true
          textBefore.term = textBeforeTerm;
          listToPushTo.push(textBefore);
        }

        let headlineCandidateElement = headlineCandidate[0];
        lastIndex = candidateIndex + headlineCandidateElement.length;

        if (headlineCandidateElement != '') {

          let enrichedTextPart = new TextPart();
          enrichedTextPart.is_plain_text = false;
          enrichedTextPart.doc_id = doc_id;

          const parser = new DOMParser();
          const doc = parser.parseFromString(headlineCandidateElement, "application/xml");
          let termObj = doc.getElementsByTagName('termobj')[0];
          // console.log(termObj)

          enrichedTextPart.concept = EnrichComponent.cleanTermObjAttrValue(termObj.getAttribute('concept')!);
          enrichedTextPart.concept_type_name = EnrichComponent.cleanTermObjAttrValue(termObj.getAttribute('concept-type-name')!);
          enrichedTextPart.presentation_type = EnrichComponent.cleanTermObjAttrValue(termObj.getAttribute('presentation-type')!);
          enrichedTextPart.prediction = +EnrichComponent.cleanTermObjAttrValue(termObj.getAttribute('prediction')!);
          enrichedTextPart.rewritten_term = EnrichComponent.cleanTermObjAttrValue(termObj.getAttribute('rewritten-term')!);
          enrichedTextPart.term_misspelled = JSON.parse(EnrichComponent.cleanTermObjAttrValue(termObj.getAttribute('term-misspelled')!));
          enrichedTextPart.term = EnrichComponent.cleanTermObjAttrValue(termObj.getAttribute('term')!);

          listToPushTo.push(enrichedTextPart);
        }

      }
      if (new_sentence.length > lastIndex) {
        let textAfterTerm = new_sentence.slice(lastIndex, new_sentence.length);
        textAfterTerm = textAfterTerm.trim()
        if (textAfterTerm != '') {

          let textAfter = new TextPart();
          textAfter.is_plain_text = true
          textAfter.term = textAfterTerm;
          listToPushTo.push(textAfter);
          if (i !== 0) {
            let textAfter = new TextPart();
            textAfter.is_plain_text = true
            textAfter.term = '<br \>';
            listToPushTo.push(textAfter);
          }
        }
      }

    }

    return outputdata;


  };

  private highlightRewrittenTerms(reponsedata: any) {
    let outputdataRewritten = {};
    // const doc_headline = reponsedata.doc_headline;
    const doc_sentences = reponsedata.doc_sentences;
    const doc_sentences_predicted = reponsedata.doc_sentences_predicted;

    let htmlRewrittenWords = '<table class="rewritten-words">';

    htmlRewrittenWords += '<tr><th style="white-space: nowrap">#</th><th>Original sentence</th><th>Rewritten sentence for prediction</th></tr>';

    let rewrittenRegex = new RegExp('\\w+_DO_NOT_PREDICT', "g");

    const nonAlphaNumRegex = /[\W_]+/g;

    let valid_doc_sentences = [];
    for (let i = 0; i < doc_sentences.length; i++) {
      let sentence = doc_sentences[i];
      // Skip sentences that consist of one emoji only, since they are filtered out/missing in the enriched sentences
      // and creates a missmatch in sentence index.
      const emojiRegex = /\P{Emoji}/u;
      if (emojiRegex.test(sentence)) {
        valid_doc_sentences.push(sentence);
      }
    }

    for (let i = 0; i < valid_doc_sentences.length; i++) {
      let sentence = valid_doc_sentences[i];


      const sentence_rewritten = doc_sentences_predicted[i];

      const is_sentence_rewritten = sentence_rewritten.replace(nonAlphaNumRegex, "").toString() !== sentence.replace(nonAlphaNumRegex, "").toString();

      let new_sentence = '';

      htmlRewrittenWords += '<tr>';
      htmlRewrittenWords += '<td><strong>' + (i + 1) + '. ' + '</strong></td>';
      htmlRewrittenWords += '<td>' + sentence + '</td>';
      if (is_sentence_rewritten) {
        htmlRewrittenWords += '<td class="rewritten-sentence">';
      } else {
        htmlRewrittenWords += '<td>';
      }
      new_sentence = sentence_rewritten.replace(rewrittenRegex, '<span class="rewritten-word">$&</span>');
      htmlRewrittenWords += new_sentence;
      htmlRewrittenWords += '</td>';
      htmlRewrittenWords += '</tr>';
      // console.log('Sentence idx: ' + (i + 1) + ', sentence: ' + doc_sentences[i] + ', sentence_rewritten: ' + doc_sentences_predicted[i])

    }

    htmlRewrittenWords += '</table>';
    // outputdataRewritten.html_rewritten_words = htmlRewrittenWords;

    // return outputdataRewritten;
    return htmlRewrittenWords;
  };


  private static cleanTermObjAttrValue(str: string) {
    return str.replaceAll('\'', '')
  }


  private static crossBrowserEndsWith(str: string, endstr: string) {
    let lastIndex = str.lastIndexOf(endstr);
    let strlen = str.length;
    let strdiff = strlen - (lastIndex + endstr.length);
    return strdiff === 0;
  }


  getRandomJobListing(): void {
    this.isShowSentencesVisible = false;
    this.resetEnrichedData();
    this.dataService.getRandomJobListing().subscribe(
      jobadForInput => {
        this.handleAdInputReturnValue(jobadForInput);
      }
    );
  }

  public getDemoHtmlJobAdByDocIdInput(): void {
    this.isShowSentencesVisible = false;
    this.resetEnrichedData();
    console.log('getDemoHtmlJobAdByDocIdInput - getting JobAd, id: ' + this.doc_id_input)

    this.dataService.getDemoHtmlJobAdByDocIdInput(this.doc_id_input).subscribe(
      jobadForInput => {
        this.handleAdInputReturnValue(jobadForInput);
      }
    );

  }

  private handleAdInputReturnValue(jobadForInput: AdInput) {
    if (!jobadForInput) {
      this.doc_id_input = "";
      this.doc_headline_input = "";
      this.doc_text_input = "";
    } else {
      this.doc_id_input = jobadForInput.doc_id_input;
      this.doc_headline_input = jobadForInput.doc_headline_input;
      this.doc_text_input = jobadForInput.doc_text_input;
    }
  }

  isInputValid(): boolean {
    // console.log('isInputValid called: ' + $scope.doc_headline_input)
    let is_valid = true;

    const doc_headline_input = this.doc_headline_input === undefined ? '' : this.doc_headline_input;
    const doc_text_input = this.doc_text_input === undefined ? '' : this.doc_text_input;

    if (doc_headline_input.trim() === '' && doc_text_input.trim() === '') {
      is_valid = false;
    }
    // console.log('isInputValid called, is_valid:' + is_valid);
    return is_valid;
  };

  isInputByIdValid(): boolean {
    let is_valid = true;
    const doc_id_input = this.doc_id_input === undefined ? '' : this.doc_id_input;
    if (doc_id_input.trim() === '') {
      is_valid = false;
    }
    return is_valid;
  };


}
