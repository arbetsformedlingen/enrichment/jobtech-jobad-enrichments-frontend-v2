import {Component} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {faCode} from '@fortawesome/free-solid-svg-icons';
import {DebugService} from "./services/debug.service";
import {DataService} from "./services/data.service";
import {version} from "./version";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent {
  constructor(private activatedroute: ActivatedRoute, private debugService: DebugService, public dataService: DataService) {
  }

  faCode = faCode;

  frontendVersion = version.frontendVersion;
  isdebug: boolean = false;

  // debug flag used to show/hide debug information in the GUI. To activate debug,
  // call the GUI with querystring debug=true, for example: http://localhost:4200/?debug=true
  sub = this.activatedroute.queryParamMap
    .subscribe(params => {
      this.isdebug = params.get('debug')?.toLowerCase() == 'true';
      console.log('this.debug:' + this.isdebug);
      this.debugService.setIsDebug(this.isdebug);
    });

  title = 'jobtech-jobad-enrichments-frontend-v2';


  chooseEnvironment($event: any) {
    this.dataService.enrichBaseUrl = this.dataService.getCurrentEnrichBaseUrl();
    console.log("AppComponent. this.dataService.environmentName: " + this.dataService.environmentName)
    console.log("AppComponent. this.dataService.enrichBaseUrl: " + this.dataService.enrichBaseUrl)
  }
}
