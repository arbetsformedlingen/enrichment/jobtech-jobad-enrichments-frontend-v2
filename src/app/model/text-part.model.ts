export class TextPart {
//  <app-term doc-id="'${doc_id}'" concept="'${concept}'" concept-type-name="'${conceptTypeName}'"
//  presentation-type="'${presentationType}'" prediction="'${prediction}'" rewritten-term="'${rewrittenTerm}'" term-misspelled="'${termMisspelled}'" term="'${term}'" ></app-term>
  doc_id: string = "";
  concept: string = "";
  concept_type_name: string = "";
  presentation_type: string = "";
  prediction: number = 0.0;
  rewritten_term: string = "";
  term_misspelled: boolean = false;
  term: string = "";
  is_plain_text: boolean = false;
}
