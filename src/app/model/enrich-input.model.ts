export class EnrichInput {
  doc_id_input: string = "n/a";
  doc_headline_input: string  = "n/a";
  doc_text_input: string = "n/a";
  is_binary_enrichment: boolean = false;
}
