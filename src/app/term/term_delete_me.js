'use strict';

function TermController($scope, $rootScope, $log, $http) {
	"ngInject";

	$scope.suggested = false;
	$scope.loading = false;


    this._getFilteredSynonyms = (conceptLabel, term, synonymTypePropname) => {
      // $log.info('Getting synonyms for conceptLabel: ' + conceptLabel + ' and term: ' + term);
      let all_enriched_candidates_synonyms = [];
      let filtered_synonyms = [];
      if ($rootScope.synonymdata.hasOwnProperty(synonymTypePropname)) {
        for (let typePropname in $rootScope.synonymdata[synonymTypePropname]) {
          // $log.info('typePropname: ' + typePropname);
          if ($rootScope.synonymdata[synonymTypePropname].hasOwnProperty(typePropname)) {
            const synonym_objs = $rootScope.synonymdata[synonymTypePropname][typePropname];
            for (let synonymobj of synonym_objs) {
              // $log.info(synonymobj);
              all_enriched_candidates_synonyms.push(synonymobj);
            }
          }
        }

        filtered_synonyms = all_enriched_candidates_synonyms.filter(function (el) {
          return el.concept_label === conceptLabel && el.term !== term;
        });

        filtered_synonyms.sort((a, b) => (a.term > b.term) ? 1 : -1);
      }
      return filtered_synonyms;
    };


	this.$onInit = () => {

		$scope.classes = "c-pointer link pa1 br2 bb bw3";
		$scope.styles = "";
		// $scope.popupclasses = "bg-dark-pink";
		$scope.concept_type_classes = "pa2 white br2 mh2";

		// $log.info('this.prediction: ' + this.prediction);

    $scope.synonymdata = {};

    $scope.synonymdata.synonyms = this._getFilteredSynonyms(this.concept, this.term, 'enriched_candidates_synonyms');
    $scope.synonymdata.misspelledsynonyms = this._getFilteredSynonyms(this.concept, this.term, 'enriched_candidates_misspelled_synonyms');


    if (this.prediction > 0.5) {
      let percent_over_middle = this.prediction - 0.5;
      const candidate_saturation = Math.round(percent_over_middle * 2 * 100);
      $scope.candidate_saturation = candidate_saturation;
      $scope.styles = "background-color:hsla(110," + candidate_saturation + "%,60%,1.0);";


    } else {
      const candidate_saturation = Math.round((0.5 - this.prediction) * 2 * 100);
      $scope.candidate_saturation = candidate_saturation;

      const candidate_lightness = (this.prediction > 0.4 && this.prediction < 0.6) ? 50 : 40;
      $scope.styles = "background-color:hsla(0," + candidate_saturation + "%, " + candidate_lightness + "%,1.0);";

      $scope.classes = $scope.classes + " white"

    }

    // $log.info('$scope.styles: ' + $scope.styles)

    // $log.info('this.conceptTypeName: ' + this.conceptTypeName);
		// if (this.demanded === 'true') {
		// if (this.prediction > 0.8) {
    if (this.conceptTypeName === "KOMPETENS") {
      const competenceClasses = " bg-hot-pink b--hot-pink";
      $scope.classes = $scope.classes + competenceClasses;
      $scope.concept_type_classes = $scope.concept_type_classes + competenceClasses;
    }
    if (this.conceptTypeName === "YRKE") {
      const occupationClasses = " bg-blue b--blue";
      $scope.classes = $scope.classes + occupationClasses;
      $scope.concept_type_classes = $scope.concept_type_classes + occupationClasses;
    }
    if (this.conceptTypeName === "FORMAGA") {
      const traitClasses = " bg-gold b--gold";
      $scope.classes = $scope.classes + traitClasses;
      $scope.concept_type_classes = $scope.concept_type_classes + traitClasses;
    }
    if (this.conceptTypeName === "GEO") {
      const geoClasses = " bg-dark-green b--dark-green";
      $scope.classes = $scope.classes + geoClasses;
      $scope.concept_type_classes = $scope.concept_type_classes + geoClasses;
    }
		// } else {
		// 	// $scope.classes = $scope.classes + " ba";
		// 	if (this.type === "KOMPETENS") {
		// 		$scope.classes = $scope.classes + " b--dark-pink";
		// 	}
		// 	if (this.type === "YRKE") {
		// 		$scope.classes = $scope.classes + " b--dark-blue";
		// 	}
		// 	if (this.type === "FORMAGA") {
		// 		$scope.classes = $scope.classes + " b--orange";
		// 	}
		// }
	};

	// $scope.suggestClicked = (reason) => {
	// 	$scope.error = null;
    //
	// 	const data = {
	// 		term: {
	// 			name: this.term,
	// 			type: this.type
	// 		},
	// 		reason: reason,
	// 		vacancy_id: this.vacancyId
	// 	};
    //
	// 	const url = "http://sauron.ws.ams.se:5050/suggest_term/delete";
	// 	$scope.loading = true;
	// 	$http.post(url, data).then(response => {
    //
	// 		$scope.loading = false;
	// 		$scope.suggested = true;
	// 		console.log(response)
	// 	}, error => {
	// 		console.log("Suggest concept for deletion failed with: ");
	// 		console.log(error);
	// 		$scope.loading = false;
	// 		$scope.error = error
	// 	})
	// }
}


angular.module("jobadEnrichmentsApp").component("term", {
	templateUrl: "views/term.html",
	controller: TermController,
	bindings: {
		vacancyId: "<",
		term: "<",
		concept: "<",
		conceptTypeName: "<",
    presentationType: "<",
		demanded: "<",
		prediction: "<",
    rewrittenTerm: "<",
    termMisspelled: "<",
    synonyms: "<",
    misspelledSynonyms: "<"
	}
});
