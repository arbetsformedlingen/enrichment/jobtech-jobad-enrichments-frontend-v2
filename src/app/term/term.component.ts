import {Component, Input, OnInit} from '@angular/core';
import {DataService} from "../services/data.service";
import {DebugService} from "../services/debug.service";

let synonyms: any[] = [];
let misspelledsynonyms: any[] = [];

@Component({
  selector: 'app-term',
  templateUrl: './term.component.html',
  styleUrls: ['./term.component.sass']
})
export class TermComponent implements OnInit{
  @Input() term: string = "";
  @Input() rewrittenTerm: string = "";
  @Input() termMisspelled: boolean = false;
  @Input() concept: string = "";
  @Input() presentationType: string = "";
  @Input() prediction: number = 0.0;
  @Input() is_plain_text: boolean = false;

  @Input() classes: string = "";
  @Input() styles: string = "";
  @Input() concept_type_classes: string = "";

  @Input() candidate_saturation: number = 0.0;


  public synonymdata = {
    "synonyms": synonyms,
    "misspelledsynonyms": misspelledsynonyms
  }

  constructor(public dataService: DataService, private debugService: DebugService) {
  };

  hasSynonyms():boolean {
    return this.synonymdata.synonyms.length > 0 || this.synonymdata.misspelledsynonyms.length > 0;
  }

  private _getFilteredSynonyms(conceptLabel: string, term: string, synonymTypePropname: string) {
    // console.log('Getting synonyms for conceptLabel: ' + conceptLabel + ' and term: ' + term);
    let all_enriched_candidates_synonyms = [];
    let filtered_synonyms = [];
    if (this.dataService.synonymdata.hasOwnProperty(synonymTypePropname)) {
      for (let typePropname in this.dataService.synonymdata[synonymTypePropname]) {
        // console.log('typePropname: ' + typePropname);
        if (this.dataService.synonymdata[synonymTypePropname].hasOwnProperty(typePropname)) {
          const synonym_objs = this.dataService.synonymdata[synonymTypePropname][typePropname];
          for (let synonymobj of synonym_objs) {
            // console.log(synonymobj);
            all_enriched_candidates_synonyms.push(synonymobj);
          }
        }
      }

      filtered_synonyms = all_enriched_candidates_synonyms.filter(function (el) {
        return el.concept_label === conceptLabel && el.term !== term;
      });

      filtered_synonyms.sort((a, b) => (a.term > b.term) ? 1 : -1);
    }
    return filtered_synonyms;
  };


  ngOnInit(): void {

    console.log("TermComponent, ngOnInit: dataService.showBinarySynonyms:" + this.dataService.showBinarySynonyms)

    this.classes = "c-pointer link pa1 br2 bb textpart";
    this.concept_type_classes = "pa2 white br2 mh2";

    this.synonymdata.synonyms = this._getFilteredSynonyms(this.concept, this.term, 'enriched_candidates_synonyms');
    this.synonymdata.misspelledsynonyms = this._getFilteredSynonyms(this.concept, this.term, 'enriched_candidates_misspelled_synonyms');


    if (this.prediction >= 0.0) {
      this.classes = this.classes + " textpart-with-prediction";
      if (this.prediction > 0.5) {
        let percent_over_middle = this.prediction - 0.5;
        const candidate_saturation = Math.round(percent_over_middle * 2 * 100);
        this.candidate_saturation = candidate_saturation;
        this.styles = "background-color:hsla(110," + candidate_saturation + "%,60%,1.0);";
      } else {
        const candidate_saturation = Math.round((0.5 - this.prediction) * 2 * 100);
        this.candidate_saturation = candidate_saturation;
        const candidate_lightness = (this.prediction > 0.4 && this.prediction < 0.6) ? 50 : 40;
        this.styles = "background-color:hsla(0," + candidate_saturation + "%, " + candidate_lightness + "%,1.0);";

        this.classes = this.classes + " white"
      }
    } else {
      this.classes = this.classes + " white"
    }

    if (this.presentationType === "COMPETENCE") {
      const competenceClasses = " bg-hot-pink b--hot-pink";
      this.classes = this.classes + competenceClasses;
      this.concept_type_classes = this.concept_type_classes + competenceClasses;
    }
    if (this.presentationType === "OCCUPATION") {
      const occupationClasses = " bg-blue b--blue";
      this.classes = this.classes + occupationClasses;
      this.concept_type_classes = this.concept_type_classes + occupationClasses;
    }
    if (this.presentationType === "TRAIT") {
      const traitClasses = " bg-gold b--gold";
      this.classes = this.classes + traitClasses;
      this.concept_type_classes = this.concept_type_classes + traitClasses;
    }
    if (this.presentationType === "GEO") {
      const geoClasses = " bg-dark-green b--dark-green";
      this.classes = this.classes + geoClasses;
      this.concept_type_classes = this.concept_type_classes + geoClasses;
    }

    // console.log('this.presentationType: ' + this.presentationType);
    // console.log('this.styles: ' + this.styles)
    // console.log('this.classes: ' + this.classes)



  }

}

